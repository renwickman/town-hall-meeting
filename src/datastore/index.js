const {Datastore} = require('@google-cloud/datastore')
const express = require('express')
const cors = require('cors')
const axios = require('axios');

const app = express();
app.use(express.json())
app.use(cors())

const datastore = new Datastore();

const fs = require('fs');
const promisify = require('util').promisify;

//To write to a file - Import the promisify
// WriteTextToFileAsync is called by passing data to be written
const writeFilePromise = promisify(fs.writeFile);

const WriteTextToFileAsync = async (contentToWrite) => {

    try {
        // FIXME: give your own path, this is just a dummy path
        const path = "./report.txt";
        await writeFilePromise(path, contentToWrite);
        
    } catch(err) {
        throw new Error(`Could not write file because of ${err}`);
    }
}


//create a new message
// also verify if sender & receiver both exist
app.post('/submitissue', async(req, res) => {
    console.log("/submitissue - posting an issue");
    const key = datastore.key('issues');
    const obj = req.body;
    const response = await datastore.save({key:key,data:obj})
    res.send(response);
})

//TODO
app.get('/issues/report', async(req, res) => {
    
    const query = datastore.createQuery('issues');
    const [data] = await datastore.runQuery(query);

    let dictNoOfType = {};
    let dictDateOfIssuePosted = {};
    let dictDateOfIssuePostedWeek = {};
    let reviewedCount = 0;
    let notReviewedCount = 0;
    
    let countNoIssues = 0;
    let date = new Date() 
    let today = new Date()
    let yesterday = new Date(date.setDate(date.getDate() - 1));
    let before7Daysdate = new Date(date.setDate(date.getDate() - 6));
    let noOfIssuesInLast24Hours = 0;
    let noOfIssuesPastWeek = 0;

    for(let eachData of data)
    {
        countNoIssues++;
        let eachPostingDay = new Date(eachData.date_of_posting);
        eachData.type = eachData.type.trim();

        if( eachPostingDay.getTime() > yesterday.getTime() && eachPostingDay.getTime() <=  today.getTime())
        {
            noOfIssuesInLast24Hours++;
            //update dictionary to get date
            let keyVal = ""+eachData.type;
            if(dictDateOfIssuePosted[keyVal] === undefined)
            {
                dictDateOfIssuePosted[keyVal] = 1;
            }
            else {
                let count = dictDateOfIssuePosted[keyVal]
                dictDateOfIssuePosted[keyVal ] = ++count;
            }
        }
        if(before7Daysdate.getTime() <= eachPostingDay.getTime())
        {
            let keyVal = ""+eachData.type;

            if(dictDateOfIssuePostedWeek[keyVal] === undefined)
            {
                dictDateOfIssuePostedWeek[keyVal] = 1;
            }
            else {
                let count = dictDateOfIssuePostedWeek[keyVal];
                dictDateOfIssuePostedWeek[keyVal] = ++count;
            }
            
            noOfIssuesPastWeek++;
        }
        //update count of issues that are reviewed and not Reviewed
        eachData.reviewed? reviewedCount++ : notReviewedCount++;
        
       
        if(dictNoOfType[eachData.type] === undefined) 
        {
            const keyVal = ""+eachData.type;
            dictNoOfType[keyVal] = 1;
        }
        else 
        {
            let count = dictNoOfType[eachData.type];
            ++count;
            dictNoOfType[eachData.type] = count;
        }
    }
    
    const values = Object.values(dictNoOfType);
    const issueType24Hours = Object.values(dictDateOfIssuePosted);
    const issueTypeWeek = Object.values(dictDateOfIssuePostedWeek)

    
    const reportObj = {
        "Total_No_Issues" : countNoIssues,
        "No_Of_Issues_Past_24_Hours" : noOfIssuesInLast24Hours,
        "No_of_Issues_Past_Week" : noOfIssuesPastWeek,
        "No_Of_Issues_Reviewed" : reviewedCount,
        "No_Of_Issues_Not_Reviewed": notReviewedCount,
        "No_Of_Issues_Type_Safety": values[0],
        "No_Of_Issue_Type_Noise": values[1],
        "No_Of_Issue_Type_Infrastructure": values[2],
        "No_Of_Issue_Type_PublicHealth": values[3],
        "No_Of_Issue_Type_Pollution": values[4],
        "No_Of_Issue_Type_Other": values[5],
        "Issue_type_SAFETY_24Hours" : issueType24Hours[0] ? issueType24Hours[0] : 0,
        "Issue_type_NOISE_24Hours" : issueType24Hours[1]? issueType24Hours[1] : 0,
        "Issue_type_INFRASTRUCTURE_24Hours" : issueType24Hours[2] ? issueType24Hours[2] : 0,
        "Issue_type_PUBLIC-HEALTH_24Hours" : issueType24Hours[3] ? issueType24Hours[3] : 0,
        "Issue_type_POLLUTION_24Hours" : issueType24Hours[4] ? issueType24Hours[4] : 0,
        "Issue_type_OTHER_24Hours" : issueType24Hours[5] ? issueType24Hours[5] : 0,
        "Issue_type_SAFETY_THIS_WEEK" : issueTypeWeek[0] ? issueTypeWeek[0] : 0,
        "Issue_type_NOISE_THIS_WEEK" : issueTypeWeek[1] ? issueTypeWeek[1] : 0,
        "Issue_type_INFRASTRUCTURE_THIS_WEEK" : issueTypeWeek[2] ? issueTypeWeek[2] : 0,
        "Issue_type_PUBLIC-HEALTH_THIS_WEEK" : issueTypeWeek[3] ? issueTypeWeek[3] : 0,
        "Issue_type_POLLUTION_THIS_WEEK" : issueTypeWeek[4] ? issueTypeWeek[4] : 0,
        "Issue_type_OTHER_THIS_WEEK" : issueTypeWeek[5] ? issueTypeWeek[5] : 0
    }

    await WriteTextToFileAsync(JSON.stringify(reportObj));
    res.send(reportObj);

})


// get all isssues by different req params
app.get('/issues', async (req, res)=>{

    //by date of posted, dateof issue, type, reviewed
    const datePosted = req.query.dateposted;
    const dateOfIssue = req.query.dateofissue;
    const issueType = req.query.type;
    const reviewed = req.query.reviewed;
    const highlighted = req.query.highlighted;

    if(datePosted !== undefined && dateOfIssue === undefined)
    {
        console.log("getting all issues by date Posted");
        const query = datastore.createQuery('issues').filter("date_of_posting" , "=", new Date(datePosted));
        const [data] = await datastore.runQuery(query);
        res.send(data);
    }
    else if(dateOfIssue!== undefined && datePosted === undefined)
    {
        console.log("getting all issues by date issued");
        const query = datastore.createQuery('issues').filter('date_of_issue', "=", new Date(dateOfIssue) );
        const [data] = await datastore.runQuery(query);
        res.send(data);
    }
    else if(issueType !== undefined)
    {
        //get issue by type
        console.log("get issue by type");   
        const query = datastore.createQuery('issues').filter('type', "=", issueType);
        const [data] = await datastore.runQuery(query);
        res.send(data);
    }
    else if(reviewed !== undefined)
    {  
        console.log("getting all issues by 'reviewed'"); 

        let isTrueSet = (reviewed === 'true');
        const query = datastore.createQuery('issues').filter('reviewed', "=", isTrueSet);
        const [data] = await datastore.runQuery(query);
        res.send(data);
    }
    else if(highlighted !== undefined) {
        console.log("getting all issues by 'highlighted'"); 

        let isTrueSet = (highlighted === 'true');
        const query = datastore.createQuery('issues').filter('highlighted', "=", isTrueSet);
        const [data] = await datastore.runQuery(query);
        res.send(data);
    }
    else {
        //get all issues
        console.log("get all issues");
        const query = datastore.createQuery('issues')
        const [data] = await datastore.runQuery(query);
        res.send(data);
    }
});


const PORT = process.env.PORT || 3002;
app.listen(PORT,()=>console.log('Application Started'));
