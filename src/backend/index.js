const express = require('express')

const app = express()

let counter = 0;
app.get('/funkytown', (req,res)=>{
    console.log('Hello')// In docker anything printed to stdout or stderr is stored in docker logs you can view
    res.send('Welcome Funky Town')
})

const PORT = process.env.PORT || 3000;
app.listen(PORT,()=>console.log('Application Started'))