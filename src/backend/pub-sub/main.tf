provider "google" {
    project = "funky-town-326721"
    region = "us-central1"
    zone = "us-central1-c"
}

resource "google_pubsub_topic" "issue-topic"{
    name = "issue"
}

resource "google_pubsub_subscription" "issue_subscription" {
    name = "issue_subscription"
    topic = google_pubsub_topic.issue-topic.name
}