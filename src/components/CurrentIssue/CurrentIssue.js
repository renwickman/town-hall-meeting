

import React ,{useState, useEffect} from "react";
import axios from "axios";
import "./CurrentIssue.css";
import logo from '../../images/IssueAnalysisPageLogo.jpeg';


export default function CurrentIssue() {

  const [issues, setIssues] = useState('');
  const [reviewed, setReviewed] = useState('');
  const [highlighted, setHighlighted] = useState('');

  useEffect( () => {
    axios.get('https://pub-sub-subscriber-7npnpkiefa-uc.a.run.app');
}, [])

  async function loadData(){
    const result = await  axios.get(`http://35.247.31.110/issues`);
    setIssues(result.data);
  }


  async function getIssuesReport(res, req){
    const result = await  axios.get(`http://35.247.31.110/issues/report`);
    //alert("File Written Successfully")
    if(result)
    {
      alert(JSON.stringify(result.data));
    }
    
  }

  loadData();
  

  return (
    <div>
      <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="#">
        <img src={logo} width="80" height="90" class="d-inline-block align-top" alt=""></img>
          <span style={{fontFamily: "'Bangers', cursive"}}>Issue Analysis Page </span>
        
        </a>
      </nav>

      <div>
        <button onClick={getIssuesReport}>Get Issues Report</button>
      </div>

      {issues ? issues.map((issue, index) =>{

        return (
          
          <div class="container"> 
          <div class="card border-info mb-3">
            <div  class="card-body" >
              <div class=" card-footer" style={{display: "flex", fontFamily: "'Yeseva One'" , fontSize: '32px'}}>  ISSUE {index+1}
              {issue.reviewed === true?<button class="btn btn-secondary" style={{ marginLeft: "auto"}} > Closed Issue </button> : <button class="btn btn-primary" style={{marginLeft: "auto"}}> Open Issue </button>}
              </div>
              <p class="card-text" >
                <span style={{fontFamily: "'Yeseva One'", fontSize: '28px'}}>{issue.issue_description}</span>
              </p>
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item" style={{display: "flex"}}>  Marked
                <ul>
                  <ul>{issue.highlighted === true ? <span class="p-3 mb-2 bg-warning text-dark">HIGHLIGHTED </span> : <span class="p-3 mb-2 bg-secondary text-light">NOT HIGHLIGHTED</span>}</ul>
                </ul>
              </li>
              <li class="list-group-item"> LOCATION 
                <ul>
                  <li>{issue.location}</li>
                </ul>
              </li>
              <li class="list-group-item"> DATE OF ISSUE
                <ul>
                  <li>{issue.date_of_issue}</li>
                </ul>
              </li>
              <li class="list-group-item"> DATE OF POSTING
                <ul>
                  <li>{issue.date_of_posting}</li>
                </ul>
              </li>  
            </ul>
          </div>
          </div>
        )    /*end of return */

      })   : ""    /*end of condition*/ }

    </div>
    
  );
}
