import React, {useState} from "react";
import axios from "axios";

import "antd/dist/antd.css";
import { DatePicker } from "antd";
import moment from 'moment-timezone';
import IssuesTable from './issue-table'
import { placeholder } from "@babel/types";
export default function IssueAnalysis() {
    const [highlighted, setHighlighted] = useState('');
    const [typeOfIssue, setTypeOfIssue] = useState('Infrastructure');
    const [dateOfIssue, setDateOfIssue] = useState('');
    const [dateOfIssuePosted, setDateOfIssuePosted] = useState('');
    const [displayObject, setDisplayObject] = useState('')
    const [reviewed, setReviewed ] = useState('')
    moment.tz.setDefault("America/Los_Angeles");

    function onIssueTypeChange(e){
      setTypeOfIssue(e.target.value);
    }

    function onReviewChange(e){
        setReviewed(e.target.value)
    }
    function onHighlightedChange(e){
        setHighlighted(e.target.value)
    }
    function handleOnChange(e){
      setDateOfIssue(e.target.value)
    }
    function handleOnChange2(e){
      setDateOfIssuePosted("" + e.target.value)
    }
    
    async function onClickByType(){
        const response = await axios.get(`http://35.247.31.110/issues?type=${typeOfIssue}`)
        setDisplayObject(response.data)
    }
    async function onClickByDateOfIssuePosted(){
        const response = await axios.get(`http://35.247.31.110/issues?dateposted=${dateOfIssuePosted}`)
        setDisplayObject(response.data)
        console.log(response)
      }
    async function onClickDateOfIssue(){
        const response = await axios.get(`http://35.247.31.110/issues?dateofissue=${dateOfIssue}`)
        setDisplayObject(response.data)
        console.log(response)
      }
    async function onClickReviewed(){
        const response = await axios.get(`http://35.247.31.110/issues?reviewed=${reviewed}`)
        setDisplayObject(response.data)
    }
    async function onClickHighlighted(){
        console.log(highlighted)  
        const response = await axios.get(`http://35.247.31.110/issues?highlighted=${highlighted}`)
        setDisplayObject(response.data)
        
    }

    return(
        <div class ="container">
          
          <form>
            <div className = "form-group">
              <label>Date of issue</label>
                <input placeholder="date" value={dateOfIssue} onChange={handleOnChange}></input>
                {/* <DatePicker  format="YYYY-MM-DDThh:mm:ss.sTZD" selected={dateOfIssue} onChange={date => {
                  console.log(new Date(date).toISOString());
                  setDateOfIssue(new Date(date).toISOString());
                  } } /> */}
                <button type="button" class="btn btn-secondary" onClick={onClickDateOfIssue}>Click Me For Date Of Issues!</button>
            </div>
    
            <div>
              <label>Date of Posting Issue</label>
              <input placeholder="date" value={dateOfIssuePosted} onChange={handleOnChange2}></input>
                {/* <DatePicker format="YYYY-MM-DDThh:mm:ss.sTZD" selected={dateOfIssuePosted} onChange={date => setDateOfIssuePosted(date)} /> */}
                <button type="button" class="btn btn-success" onClick={onClickByDateOfIssuePosted}>Click Me For Date Of Issue Posted!</button> 
            </div>
            <div className="form-group">
              <label>Issue Reviewed
                <input class="form-control" onChange={onReviewChange} type="text" placeholder="true or false"/>
                <button type="button" class="btn btn-danger" onClick={onClickReviewed}>Click Me For Reviewed!</button>
              </label>
            </div>
            <div className="form-group">
              <label>
                Issue Type
              <select onSelect={onIssueTypeChange} onChange={onIssueTypeChange} class="form-control" name="issueType" id="issueType">
                <option value="Infrastructure">Infrastructure</option>
                <option value="Safety">Safety</option>
                <option value="PublicHealth"> Public Health</option>
                <option value="Pollution"> Pollution</option>
                <option value="Noise"> Noise </option>
                <option value="Other"> Other </option>
              </select>
              <button type="button" class="btn btn-info" onClick={onClickByType}>Click Me For Type Of Issues!</button>
              </label>
            </div>
            <div className="form-group">
            <label>
              Priority / Highlighted
              <input class="form-control" type="text" name="highlighted" 
                  onChange={onHighlightedChange} />
                  <button type="button" class="btn btn-primary" onClick={onClickHighlighted}>Click Me For Highlighted!</button>
            </label>
            {/* <button type="button" class="btn btn-info" onClick={onClickByType}>Click Me For Type Of Issues!</button>
            <button type="button" class="btn btn-secondary" onClick={onClickDateOfIssue}>Click Me For Date Of Issues!</button>
            <button type="button" class="btn btn-success" onClick={onClickByDateOfIssuePosted}>Click Me For Date Of Issue Posted!</button>
            <button type="button" class="btn btn-danger" onClick={onClickReviewed}>Click Me For Reviewed!</button>
            <button type="button" class="btn btn-primary" onClick={onClickHighlighted}>Click Me For Highlighted!</button> */}
            {
                displayObject ? <IssuesTable issues={displayObject}></IssuesTable> : ""
            } 
            </div>
          </form>
        </div>
      );
}