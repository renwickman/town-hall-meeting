import React, {useState} from "react";
import styled from 'styled-components'

const Styles = styled.div`
  padding: 1rem;

  table {
    border-spacing: 0;
    border: 1px solid black;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
  }
`

export default function IssuesTable(props){


    const issues = props.issues
    return(
    <div>
        <Styles>
    <table>
        <thead><tr>
        <th>Type of Issue</th>
        <th>Date Posted</th>
        <th>Date of Issue</th>
        <th>Reviewed</th>
        <th>Highlighted</th>
             </tr>
        </thead>
        <tbody>
            {issues.map(i=><tr>
                <td>{i.type}</td>
                <td>{i.date_of_posting}</td>
                <td>{i.date_of_issue}</td>
                <td>{i.reviewed === true ? "true" : "false" }</td>
                <td>{i.highlighted === true ? "true" : "false" }</td>
            </tr>)}
        </tbody>
    </table>
    </Styles>
    </div>)


}