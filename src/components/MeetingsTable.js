import { Label } from 'reactstrap';
export default function MeetingsTable(props){
    const meetings = props.meetings;
    
    return(<table><tr><th>Meeting</th><th>Location</th><th>Time</th><th>Topic</th></tr>
        <tbody>
        {meetings.map(m => <tr key={m.meeting_id}>
                    <td>{m.meeting_id}</td>
                    <td>{m.location}</td>
                    <td>{m.time}</td>
                    <td>{m.topic}</td>
                    </tr>)}
        </tbody>
        </table>)
    }