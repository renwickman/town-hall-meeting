import { meetingsDAO } from "../DAOs/meetings-dao";
import { MeetingsDaoPostgress } from "../DAOs/meetings-dao-postgres";
import { Meetings } from "../entities";
import MeetingsService from "./meetings-services";

export class MeetingsServiceImpl implements MeetingsService{

    meetingsDAO:meetingsDAO = new MeetingsDaoPostgress()

    createMeeting(meeting: Meetings): Promise<Meetings> {
        return this.meetingsDAO.createMeeting(meeting);
    }
    retrieveAllMeetings(): Promise<Meetings[]> {
        return this.meetingsDAO.getAllMeetings();
    }
    retieveMeetingById(meeting_id: number): Promise<Meetings> {
        return this.meetingsDAO.getMeetingById(meeting_id);
    }
    updateMeeting(meeting: Meetings): Promise<Meetings> {
        return this.meetingsDAO.updateMeeting(meeting);
    }
    deleteMeeting(meeting_id: number): Promise<boolean> {
        return this.meetingsDAO.deleteMeetingById(meeting_id);
    }

    
}