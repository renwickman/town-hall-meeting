import { client } from "../connection";
import { Meetings } from "../entities";
import { MissingMeetingError } from "../errors";
import { meetingsDAO } from "./meetings-dao";

export class MeetingsDaoPostgress implements meetingsDAO{

    async createMeeting(meeting: Meetings): Promise<Meetings> {
        const sql:string = "insert into meetings(meeting_location,meeting_time,topics) values ($1,$2,$3) returning meeting_id;";
        const values = [meeting.location,meeting.time,meeting.topic];
        const result = await client.query(sql,values);
        meeting.meeting_id = result.rows[0].meeting_id;
        return meeting;
    }
    async getMeetingById(meeting_id: number): Promise<Meetings> {
        const sql:string = 'select * from meetings where meeting_id = $1';
        const values = [meeting_id];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingMeetingError(`The meeting with ID ${meeting_id} does not exist`);
        }
        const row = result.rows[0];
        const meeting:Meetings = new Meetings(
            row.meeting_id,
            row.meeting_location,
            row.meeting_time,
            row.topics);
        return meeting;
    }

    async getAllMeetings(): Promise<Meetings[]> {
        const sql:string = 'select * from meetings';
        const result = await client.query(sql);
        const meetings:Meetings[] = []
        for(const row of result.rows){
            const meeting:Meetings = new Meetings(
                row.meeting_id,
                row.meeting_location,
                row.meeting_time,
                row.topics);
            meetings.push(meeting);
        }
        return meetings;
    }
    async updateMeeting(meeting: Meetings): Promise<Meetings> {
        const sql:string = 'update meetings set meeting_location=$1, meeting_time=$2, topics=$3 where meeting_id=$4';
        const values = [meeting.location,meeting.time,meeting.topic,meeting.meeting_id];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingMeetingError(`The meeting with ID ${meeting.meeting_id} doed not exist`);
        }
        return meeting;
    }
    async deleteMeetingById(meeting_id: number): Promise<boolean> {
        const sql:string = 'delete from meetings where meeting_id=$1';
        const values = [meeting_id];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingMeetingError(`The meeting with ID ${meeting_id} does not exist`);
        }
        return true;
    }


}